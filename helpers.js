function ParseStringToStudent(student) {
    var studentParams = student.split(",");
    var st = {
        ime: studentParams[0],
        prezime: studentParams[1],
        index: studentParams[2],
        grupa:studentParams[3]
    }
    return st;
}

function ParseVjezbe(niz) {
    for (var i = 0; i < niz.length; i++) {
        if (niz[i].brojVjezbi) {
            niz[i].brojVjezbi = parseInt(niz[i].brojVjezbi);
            niz[i].brojZadataka = niz[i].brojZadataka.replace("{", "").replace("}", "").split("|").map(Number);
        }
    }
    return niz;
}

function csvToArray(str, delimiter = ",") {

    const headers = str.slice(0, str.indexOf("\n")).split(delimiter);
    const rows = str.slice(str.indexOf("\n") + 1).split("\n");

    const arr = rows.map(function (row) {
        const values = row.split(delimiter);
        const el = headers.reduce(function (object, header, index) {
            object[header] = values[index];
            return object;
        }, {});
        return el;
    });
    return arr;
}



module.exports={
    ParseStringToStudent:ParseStringToStudent,
    ParseVjezbe:ParseVjezbe,
    csvToArray:csvToArray
}
