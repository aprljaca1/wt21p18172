function iscrtajStudente(error, data) {
    var div = document.querySelector("#prikaz");

    var t = document.createElement("table");
    t.id = "customers";
    var headerRow = document.createElement("tr");
    var hime = document.createElement("th");
    hime.innerText = "Ime";
    var hprezime = document.createElement("th");
    hprezime.innerText = "Prezime";
    var hindex = document.createElement("th");
    hindex.innerText = "Index";
    var hgrupa = document.createElement("th");
    hgrupa.innerText = "Grupa";
    var hbutton = document.createElement("th");
    hbutton.innerText = "Edit";
    hbutton.style.alignContent = "center";
    headerRow.appendChild(hime);
    headerRow.appendChild(hprezime);
    headerRow.appendChild(hindex);
    headerRow.appendChild(hgrupa);
    headerRow.appendChild(hbutton);
    t.append(headerRow);
    data.forEach(element => {
        var row = document.createElement("tr");
        var dime = document.createElement("td");
        dime.innerText = element.ime;
        var dprezime = document.createElement("td");
        dprezime.innerText = element.prezime;
        var dindex = document.createElement("td");
        dindex.innerText = element.index;
        var dgrupa = document.createElement("td");
        dgrupa.innerText = element.grupa.naziv;
        
        var FN = document.createElement("button");
        FN.innerText="Izmjeni";
        FN.setAttribute("type", "submit");
        FN.setAttribute("name", "Index");
        FN.setAttribute("value",element.index);
        row.appendChild(dime);
        row.appendChild(dprezime);
        row.appendChild(dindex);
        row.appendChild(dgrupa);
        row.appendChild(FN);
        row.style.textAlign = "center";
        t.appendChild(row);
    });

    var form=document.createElement("form");
    form.setAttribute("method","get");
    form.setAttribute("action","postaviGrupu.html");
    form.appendChild(t);
    div.appendChild(form);
}

document.onload = StudentAjax.dohvatiSveStudente(iscrtajStudente);