export function dajTacnost(deseto)
{

  var json = JSON.parse(deseto);
  var brojTestova=json.stats.tests;
  if(brojTestova==0)
  {
    var vrati =
    {
      tacnost:'0%',
      greske:"Testovi se ne mogu izvršiti"
    }
    return vrati;
  }

  var brojUspjesnihTestova=json.stats.passes;
  var tacnost=(brojUspjesnihTestova/brojTestova)*100;
  var tacnostZaokruzena = Math.round(tacnost*10)/10;
  var greske;
  var zaVratii=[];
  if(tacnost<100)
  {
    greske=json.failures;
    for(var i=0;i<greske.length;i++)
    {
      zaVratii.push(greske[i].fullTitle);
    }
  }

  var vrati =
  {
    "tacnost":tacnostZaokruzena+'%',
    "greske":zaVratii
  };

 return vrati;
};

function vratiBrojTestova(n1, n2) {
  var brojac = 0;
  var nalaziSe = false;
  for (let i = 0; i < n1.length; i++) {
    nalaziSe = false;
    for (let j = 0; j < n2.length; j++) {
      if (n1[i] == n2[j]) nalaziSe = true;
    }
    if (!nalaziSe) brojac++;
  }
  return brojac;
}

//testovi koji padaju u rezultatu1 a ne pojavljuju se u rezultatu2
function vratiTestove(n1, n2) {
  var vrati = [];
  var nalaziSe = false;

  for (let i = 0; i < n1.length; i++) {
    nalaziSe = false;
    for (let j = 0; j < n2.length; j++) {
      if (n1[i] == n2[j]) nalaziSe = true;
    }
    if (!nalaziSe) {
      vrati.push(n1[i]);
    }
  }
  vrati.sort();
  return vrati;
}

function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}


export function porediRezultate(rezultat1, rezultat2) {
  const niz1 = [];
  const niz2 = [];

  var obj1 = JSON.parse(rezultat1);
  var obj2 = JSON.parse(rezultat2);

  for (let i = 0; i < obj1.stats.tests; i++) {
    niz1.push(obj1.tests[i].fullTitle)
  }
  for (let i = 0; i < obj2.stats.tests; i++) {
    niz2.push(obj2.tests[i].fullTitle)
  }

  //racunanje x
  var x;
  const n1 = []; //testovi koji padaju u rez1
  const n2 = []; //svi testovi rez2
  if (arraysEqual(niz1, niz2)) {
    x = dajTacnost(rezultat2).tacnost;
  }
  else {

    for (let i = 0; i < obj1.stats.failures; i++) {
      n1.push(obj1.failures[i].fullTitle);
    }
    for (let i = 0; i < obj2.stats.tests; i++) {
      n2.push(obj2.tests[i].fullTitle);
    }

    //broj testova koji padaju u rezultatu1 a ne pojavljuju se u rezultatu2
    var a = vratiBrojTestova(n1, n2);

    x = (a + obj2.stats.failures) / (a + obj2.stats.tests) * 100+'%';
  }


  //greske
  var finalniNiz = [];
  if (arraysEqual(niz1, niz2)) {
    for (let i = 0; i < obj2.stats.failures; i++) {
      finalniNiz.push(obj2.failures[i].fullTitle);
    }
    finalniNiz.sort();
  }
  else {
    //testovi koji padaju u rez1 a ne nalaze se u rez2 
    finalniNiz = vratiTestove(n1, n2);
    var testoviR2 = []
    //failures u rez2
    for (let i = 0; i < obj2.stats.failures; i++) {
      testoviR2.push(obj2.failures[i].fullTitle);
    }
    testoviR2.sort();
    for (var i = 0; i < testoviR2.length; i++) {
      finalniNiz.push(testoviR2[i]);
    }
  }
  var obj =
  {
    "promjena": x,
    "greske": finalniNiz
  };
  return obj;
}