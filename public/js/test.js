let assert=chai.assert
let div=document.getElementById("brojVjezbi");
let div1=document.getElementById("odgovorServera");
chai.should();

describe('test', function() {

  beforeEach(function() {
    this.xhr = sinon.useFakeXMLHttpRequest();
    this.requests = [];
    this.xhr.onCreate = function(xhr) {
      this.requests.push(xhr);
    }.bind(this);
  });
 
  afterEach(function() {
    this.xhr.restore();
    var divovi=[div,div1];
    for(let i=0;i<divovi.length;i++)
    {
    while(divovi[i].firstChild){
      divovi[i].removeChild(divovi[i].lastChild);
    }
  }
  });
 
  it('Test get metode sa funkcijom dohvatiPodatke', function(done) {
    var podaci = {brojVjezbi: 4, brojZadataka: [4,2,4,4]}
    let dataJson=JSON.stringify(podaci);
    VjezbeAjax.dohvatiPodatke(function(err,data) {
        let d=JSON.parse(data);
        assert.equal(d.brojVjezbi,4)
        assert.equal(d.brojZadataka[0],podaci.brojZadataka[0])
        assert.equal(d.brojZadataka[1],podaci.brojZadataka[1])
        assert.equal(d.brojZadataka[2],podaci.brojZadataka[2])
        assert.equal(d.brojZadataka[3],podaci.brojZadataka[3])
        done();
    })
    this.requests[0].respond(200,{'Content-Type':'text/json'}, dataJson)
  })

  it('Test post metode sa funkcijom posaljiPodatke', function(done) {
    var podaci = {brojVjezbi: 3, brojZadataka: [1,2,3]}
    let dataJson=JSON.stringify(podaci);
    VjezbeAjax.posaljiPodatke(dataJson, function(err, data) {
      let d=JSON.parse(data);
        assert.equal(d.brojVjezbi,3)
        assert.equal(d.brojZadataka[0],1)
        assert.equal(d.brojZadataka[1],2)
        assert.equal(d.brojZadataka[2],3)
      done();
    });
    this.requests[0].respond(200,{'Content-Type':'text/json'}, dataJson)
  })

  it('Test posaljiPodatke funckije', function(done) {
    var podaci = {brojVjezbi: 1, brojZadataka: [2]}
    var dataJson = JSON.stringify(podaci)
    VjezbeAjax.posaljiPodatke(dataJson,function(err,data) {
      data=JSON.parse(data);
      assert.equal(data.brojVjezbi,1)
      assert.equal(data.brojZadataka[0],2)
      done()  
  })
  this.requests[0].respond(200,{'Content-Type':'text/json'}, dataJson)
  }) 

  it('Test slanja neispravnih podataka 1', function(done) {
    var podaci = {brojVjezbi: 1, brojZadataka: [14]}
    var dataJson = {status:"error", data: "Pogresan parametar z14"};
    var input=document.createElement("input");
    input.setAttribute("value",14);
    VjezbeAjax.posaljiPodatke(dataJson,function(err,data) {
      assert.equal(data.status,"error");
      assert.equal(data.data,"Pogresan parametar z14");
      done()  
  })
  this.requests[0].respond(200,{'Content-Type':'text/json'}, JSON.stringify(dataJson))
  })

  it('Test slanja neispravnih podataka 2', function(done) {
    var podaci = {brojVjezbi: 16, brojZadataka: [14,123]}
    var dataJson = {status:"error", data: "Pogresan parametar 16,z14,z123"};
    var input=document.createElement("input");
    input.setAttribute("value",14);
    VjezbeAjax.posaljiPodatke(dataJson,function(err,data) {
      assert.equal(data.status,"error");
      assert.equal(data.data,"Pogresan parametar 16,z14,z123");
      done()  
  })
  this.requests[0].respond(200,{'Content-Type':'text/json'}, JSON.stringify(dataJson))
  })

  it('Test za UnosInputPolja', function() { 
    VjezbeAjax.dodajInputPolja(div,5);
    assert.equal(div.children.length/3,5);
    //Dijelim sa tri jer imam 3* broj vjezbi djece u divu.
    //Imam input labeli br tagove za svaki vjezbu.
  })

});
