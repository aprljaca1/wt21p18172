function citajPodatke(error, data) {

    if (error == null) {
        var div = document.getElementById("odabirVjezbe");
        var obj = JSON.parse(data);
        if (obj.length > 0) {
            for (var i = 0; i < obj[0].brojVjezbi; i++) {
                var parse = { brojVjezbi: i + 1, brojZadataka: obj[0].brojZadataka[i] }
                VjezbeAjax.iscrtajVjezbe(div, parse)
            }
        }
    }
}

document.onload = VjezbeAjax.dohvatiPodatke(citajPodatke);

