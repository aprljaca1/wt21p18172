const VjezbeAjax = (function () {
    function dodajInputPolja(DOMelementDIVauFormi, brojVjezbi) {
        DOMelementDIVauFormi.querySelectorAll('*').forEach(n => n.remove());
        for (var i = 0; i < brojVjezbi; i++) {
            var input = document.createElement("input");
            input.classList.add("proba");
            input.id = "z" + (i + 1);
            input.value = 4;
            var br = document.createElement("br");
            input.type = "text";

            var newlabel = document.createElement("Label");
            newlabel.classList.add("proba");
            newlabel.innerHTML = "Zadatak " + (i + 1) + ":      ";
            DOMelementDIVauFormi.appendChild(newlabel);
            DOMelementDIVauFormi.appendChild(input);
            DOMelementDIVauFormi.appendChild(br);
        }
    }

    function posaljiPodatke(vjezbeObjekat, callbackFja) {
        var xhr = new XMLHttpRequest();
        var url = 'http://localhost:3000/vjezbe';
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = () => {
            var response = JSON.parse(xhr.response);
            if (xhr.status == 200) {
                const odgovorServera = document.getElementById("odgovorServera");
                odgovorServera.innerHTML = "Vjezba dodana";
            }
            else {
                const odgovorServera = document.getElementById("odgovorServera");
                odgovorServera.innerHTML = 'Vjezba nije dodana. Greska: "' + response.data + '"';
            }
            var error = xhr.response;
            callbackFja(error, vjezbeObjekat);
        };
        xhr.send(vjezbeObjekat);
    }

    function dohvatiPodatke(callbackFja) {
        var xhr = new XMLHttpRequest();
        var url = 'http://localhost:3000/vjezbe';
        xhr.open("GET", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = () => {
            var data;
            var error;
            if (xhr.status == 200) {
                data = xhr.response;
                error = null;
            }
            else {
                data = null;
                error = xhr.response;
            }
            callbackFja(error, data);
        };
        xhr.send();
    }

    function iscrtajZadatke(vjezbaDOMelement, brojZadataka) {
        if (vjezbaDOMelement.childNodes.length === 1) {
            var div = document.createElement("div");
            div.classList.add('klasa');

            for (var i = 0; i < brojZadataka; i++) {
                var child = document.createElement("button");
                child.textContent = "Zadatak " + (i + 1);
                div.appendChild(child);
            }
            div.style.display = 'block';
            vjezbaDOMelement.appendChild(div);
        } else {
            var div = vjezbaDOMelement.childNodes[1];
            div.style.display = div.style.display === 'block' ? 'none' : 'block';
        }
    }

    function iscrtajVjezbe(divDOMElement, obj) {
        var node = document.createElement("div");
        node.classList.add("div1");
        var id = "z" + obj.brojVjezbi;
        node.id = id;
        node.textContent = "Vjezba " + obj.brojVjezbi;
        divDOMElement.appendChild(node);

        node.onclick = function () {
            iscrtajZadatke(node, obj.brojZadataka)
        };
    }

    return {
        iscrtajZadatke: iscrtajZadatke,
        iscrtajVjezbe: iscrtajVjezbe,
        dohvatiPodatke: dohvatiPodatke,
        posaljiPodatke: posaljiPodatke,
        dodajInputPolja: dodajInputPolja
    }
}());
