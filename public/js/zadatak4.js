import {dajTacnost, porediRezultate} from './testoviParser.js';
let assert = chai.assert;
var testovi=JSON.parse(m);

describe('Testovi za treci zadatak:', function(){
    it('isti rezultati bez gresaka', function(){
        var rezultat1 = JSON.stringify(testovi[0]);
        var rezultat2 = JSON.stringify(testovi[1]);
        var rezultat = porediRezultate(rezultat1, rezultat2);
        assert.equal(rezultat.promjena, '100%');
        var greske=JSON.stringify(rezultat.greske);
        assert.equal(greske,'[]');
    });

    it('isti rezultati, u drugom ima greska', function(){
        var rezultat1 = JSON.stringify(testovi[2]);
        var rezultat2 = JSON.stringify(testovi[3]);
        var rezultat = porediRezultate(rezultat1, rezultat2);
        assert.equal(rezultat.promjena, '50%');
        var greske=JSON.stringify(rezultat.greske);
        assert.equal(greske,'["Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3"]');
    });

    it('dva ista prazna rezultata', function(){
        var rezultat1 = JSON.stringify(testovi[4]);
        var rezultat2 = JSON.stringify(testovi[5]);
        var rezultat = porediRezultate(rezultat1, rezultat2);
        assert.equal(rezultat.promjena, '0%');
        var greske=JSON.stringify(rezultat.greske);
        assert.equal(greske,'[]');
    });

    it('potpuno razliciti testovi', function(){
        var rezultat1 = JSON.stringify(testovi[6]);
        var rezultat2 = JSON.stringify(testovi[7]);
        var rezultat = porediRezultate(rezultat1, rezultat2);
        assert.equal(rezultat.promjena, '75%');
        var greske=JSON.stringify(rezultat.greske);
        assert.equal(greske,'["Tabela crtaj() should draw 3 row when parameter are 3,3","Tabela crtaj() should draw 1 rows when parameter are 1,1","Tabela crtaj() should draw 3 rows when parameter are 2,3"]');
    });

    it('mix testova', function(){
        var rezultat1 = JSON.stringify(testovi[8]);
        var rezultat2 = JSON.stringify(testovi[9]);
        var rezultat = porediRezultate(rezultat1, rezultat2);
        assert.equal(rezultat.promjena, '25%');
        var greske=JSON.stringify(rezultat.greske);
        assert.equal(greske,'["Tabela crtaj() should draw 3 row when parameter are 3,3"]');
    });

});
