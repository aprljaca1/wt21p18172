function popuniElemente(error, data) {
    var parsedData = JSON.parse(data);
    document.querySelector('#ime').value = parsedData.ime;
    document.querySelector('#prezime').value = parsedData.prezime;
    document.querySelector('#index').value = parsedData.index;
    document.querySelector('#grupa').value = parsedData.grupa.naziv;

}

function getQueryString() {
    var result = {}, queryString = location.search.slice(1),
        re = /([^&=]+)=([^&]*)/g, m;

    while (m = re.exec(queryString)) {
        result[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
    }

    return result;
}

var myParam = getQueryString()["Index"];

document.onload = StudentAjax.dohvatiStudenta(myParam, popuniElemente);

document.getElementById("izmjenaGrupe").addEventListener("click", function () {
    var index = document.querySelector('#index').value;
    var grupa = document.querySelector('#grupa').value;
    StudentAjax.postaviGrupu(index, grupa, function () { });
});