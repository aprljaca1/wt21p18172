import {dajTacnost, porediRezultate} from './testoviParser.js';
let assert = chai.assert;
var testovi=JSON.parse(s);


describe('Testovi za drugi zadatak:', function(){
    it('2 padaju 2 prolaze', function(){
        var string = JSON.stringify(testovi[0]);
        assert.equal(dajTacnost(string).tacnost, '50%');
        var greske=JSON.stringify(dajTacnost(string).greske);
        assert.equal(greske,'["Tabela crtaj() should draw 3 row when parameter are 3,3","Tabela crtaj() should draw 1 columns in row 3 when parameter are 3,3"]');
    });

    it('jedan prolazi', function(){
        var string = JSON.stringify(testovi[1]);
        assert.equal(dajTacnost(string).tacnost, '33.3%');
        var greske=JSON.stringify(dajTacnost(string).greske);
        assert.equal(greske,'["Tabela crtaj() should draw 3 row when parameter are 3,3","Tabela crtaj() should draw 1 columns in row 3 when parameter are 3,3"]');
    });

    it('svi padaju', function(){
        var string = JSON.stringify(testovi[2]);
        assert.equal(dajTacnost(string).tacnost, '0%');
        var greske=JSON.stringify(dajTacnost(string).greske);
        assert.equal(greske,'["Tabela crtaj() should draw 3 row when parameter are 3,3","Tabela crtaj() should draw 1 columns in row 3 when parameter are 3,3"]');
    });

    it('svi prolaze', function(){
        var string = JSON.stringify(testovi[3]);
        assert.equal(dajTacnost(string).tacnost, '100%');
        var greske=JSON.stringify(dajTacnost(string).greske);
        assert.equal(greske,'[]');
    });

    it('nema testova', function(){
        var string = JSON.stringify(testovi[4]);
        assert.equal(dajTacnost(string).tacnost, '0%');
        var greske=JSON.stringify(dajTacnost(string).greske);
        assert.equal(greske,'"Testovi se ne mogu izvršiti"');
    });
});


