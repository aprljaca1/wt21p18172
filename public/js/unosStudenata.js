document.getElementById("unosStudenta").onclick = function () {
    var ime = document.querySelector('#ime').value;
    var prezime = document.querySelector('#prezime').value;
    var index = document.querySelector('#index').value;
    var grupa = document.querySelector('#grupa').value;
    if (!ime || !prezime || !index || !grupa) {
        const odgovorServera = document.getElementById("odgovorServera");
        odgovorServera.innerHTML = "Nijedno polje ne smije biti prazno";
    }
    else
    {
        var student = {
            ime: ime,
            prezime: prezime,
            index: index,
            grupa: grupa
        }
    
        StudentAjax.dodajStudenta(JSON.stringify(student), function () { });
    }
    
};

