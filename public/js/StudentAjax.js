const StudentAjax = (function () {
    function dodajStudenta(student, fnCallback) {
        var xhr = new XMLHttpRequest();
        var url = 'http://localhost:3000/student';
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = () => {
            var response = JSON.parse(xhr.response);
            if (xhr.status == 200) {
                const odgovorServera = document.getElementById("ajaxstatus");
                odgovorServera.innerHTML = response.status;
            }
            else {
                const odgovorServera = document.getElementById("ajaxstatus");
                odgovorServera.innerHTML = response.status;
            }
            var error = xhr.response;
            fnCallback(error, student);
        };
        xhr.send(student);
    }

    function postaviGrupu(index, grupa, fnCallback) {
        var xhr = new XMLHttpRequest();
        var url = 'http://localhost:3000/student/' + index;
        var obj = {
            grupa: grupa
        }
        xhr.open("PUT", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = () => {
            var response = JSON.parse(xhr.response);
            if (xhr.status == 200) {
                const odgovorServera = document.getElementById("ajaxstatus");
                odgovorServera.innerHTML = response.status;
            }
            else {
                const odgovorServera = document.getElementById("ajaxstatus");
                odgovorServera.innerHTML = response.status;
            }
            var error = xhr.response;
            fnCallback(error, obj);
        };
        xhr.send(JSON.stringify(obj));
    }

    function dodajBatch(csvStudenti, fnCallback) {
        var xhr = new XMLHttpRequest();
        var url = 'http://localhost:3000/batch/student';
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "text/plain");
        xhr.onload = () => {
            var response = JSON.parse(xhr.response);
            console.log(response)
            if (xhr.status == 200) {
                const odgovorServera = document.getElementById("ajaxstatus");
                odgovorServera.innerHTML = response.status;
            }
            else {
                const odgovorServera = document.getElementById("ajaxstatus");
                odgovorServera.innerHTML = response.status;
            }
            var error = xhr.response;
            fnCallback(error, csvStudenti);
        };
        xhr.send(csvStudenti);

    }

    function dohvatiStudenta(index, fnCallback) {
        var xhr = new XMLHttpRequest();
        var url = 'http://localhost:3000/student/' + index;
        xhr.open("GET", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = () => {
            var response = JSON.parse(xhr.response);
            if (xhr.status == 200) {
                
            }
            else {
               
            }

            var error = xhr.response;
            fnCallback(error, response);
        };
        xhr.send();
    }
    function dohvatiSveStudente(fnCallback) {
        var xhr = new XMLHttpRequest();
        var url = 'http://localhost:3000/student';
        xhr.open("GET", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = () => {
            var response = JSON.parse(xhr.response);
            if (xhr.status == 200) {
                
            }
            else {
                
            }

            var error = xhr.response;
            fnCallback(error, response);
        };
        xhr.send();

    }

    function IzmjeniStudenta(id) {

       
    }
    return {
        dodajStudenta: dodajStudenta,
        postaviGrupu: postaviGrupu,
        dodajBatch: dodajBatch,
        dohvatiStudenta: dohvatiStudenta,
        dohvatiSveStudente: dohvatiSveStudente,
        IzmjeniStudenta:IzmjeniStudenta
    }
}());