const Sequelize = require("sequelize");

module.exports=(sequelize,DataTypes)=>{
    const student=sequelize.define("zadatak",{
        brojZadataka:Sequelize.STRING,
    },{
        freezeTableName: true,
        timestamps: false
    });
    return student;
}