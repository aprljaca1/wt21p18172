const Sequelize = require("sequelize");

module.exports=(sequelize,DataTypes)=>{
    const student=sequelize.define("vjezba",{
        brojVjezbi:Sequelize.STRING,
    },{
        freezeTableName: true,
        timestamps: false
    });
    return student;
}