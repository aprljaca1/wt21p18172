const Sequelize = require("sequelize");

module.exports=(sequelize,DataTypes)=>{
    const grupa=sequelize.define("grupa",{
        naziv:
        {
            type:Sequelize.STRING,
            unique:true
        }
    },{
        freezeTableName: true,
        timestamps: false
    });
    return grupa;
}