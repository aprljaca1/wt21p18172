const Sequelize = require("sequelize");

module.exports=(sequelize,DataTypes)=>{
    const student=sequelize.define("student",{
        ime:Sequelize.STRING,
        prezime:Sequelize.STRING,
        index:
        {
            type:Sequelize.STRING,
            unique:true
        },
    },{
        freezeTableName: true,
        timestamps: false
    });
    return student;
}