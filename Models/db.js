const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2118172","root","root",{host:"localhost",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

db.grupa=require('./Grupa.js')(sequelize, Sequelize);
db.vjezba=require('./Vjezba.js')(sequelize, Sequelize);
db.student=require('./Student.js')(sequelize, Sequelize);
db.zadatak=require('./Zadatak.js')(sequelize, Sequelize);

db.grupa.hasMany(db.student);
db.student.belongsTo(db.grupa);

db.vjezba.hasMany(db.zadatak);
db.zadatak.belongsTo(db.vjezba);


module.exports=db;