const express = require('express');
const Sequelize = require("sequelize");
const fs = require('fs');
const path = require('path');
const app = express();
const db = require('./models/db.js');
const env = process.env.NODE_ENV || 3000;
const helpers = require('./helpers.js');
const { json } = require('express/lib/response');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.text());


const student = db.student;
const grupa = db.grupa;
const vjezba = db.vjezba;
const zadatak = db.zadatak;

app.get('/student', async (req, res) => {

    var vj = await student.findAll({ include: grupa });
    console.log("Ovo je student " + vj)
    if (!vj) {
        res.status(400).json(vj);
    }
    else {
        res.status(200).json(vj);
    }
});



app.post('/student', async (req, res) => {


    var studentDb = await student.findOne({ where: { index: req.body.index } });
    var grupaDb = await grupa.findOne({ where: { naziv: req.body.grupa } });

    if (!studentDb) {

        var noviStudent =
        {
            ime: req.body.ime,
            prezime: req.body.prezime,
            index: req.body.index,
        };

        if (!grupaDb) {
            var novaGrupa =
            {
                naziv: req.body.grupa
            };

            var gp = await grupa.create(novaGrupa);
            noviStudent.grupaId = gp.dataValues.id;
            var sp = await student.create(noviStudent);
        }
        else {
            noviStudent.grupaId = grupaDb['id'];
            var sp = await student.create(noviStudent);
        }
        res.status(200).json({ status: "Kreiran student!" });
    }
    else {
        res.status(400).json({ status: 'Student sa indexom ' + studentDb['index'] + ' već postoji!' });
    }

});


app.get('/student/:index', async (req, res) => {

    var studentDb = await student.findOne({ where: { index: req.params.index },include:grupa });
    console.log("Ovo je student " + studentDb)
    if (!studentDb) {
        res.status(400).json({ status: 'Student sa indexom ' + req.params.index + ' ne postoji!' });
    }
    else {
        res.status(200).json(JSON.stringify(studentDb));
    }
});


app.put('/student/:index', async (req, res) => {

    var studentDb = await student.findOne({ where: { index: req.params.index } });
    console.log("Ovo je student " + studentDb)
    if (!studentDb) {
        res.status(400).json({ status: 'Student sa indexom ' + req.params.index + ' ne postoji!' });
    }
    else {
        var grupaDb = await grupa.findOne({ where: { naziv: req.body.grupa } });
        if (!grupaDb) {
            var novaGrupa =
            {
                naziv: req.body.grupa
            };
            var gp = await grupa.create(novaGrupa);
            studentDb.set({
                grupaId: gp.dataValues.id
            });
            await studentDb.save();
        }
        else {
            studentDb.set({
                grupaId: grupaDb['id']
            });
            await studentDb.save();
        }
        res.status(200).json({ status: "Promjenjena grupa studentu " + studentDb['index'] });
    }
});


app.post('/batch/student', async (req, res) => {

    var studenti = req.body;
    var nizStudenata = studenti.split(/\r?\n/);
    var postojeciStudenti = [];
    for (let i = 0; i < nizStudenata.length; i++) {
        var st = helpers.ParseStringToStudent(nizStudenata[i]);
        var studentDb = await student.findOne({ where: { index: st.index }});
        if (studentDb != null) {
            postojeciStudenti.push(st);
        }
        else {
            var noviStudent =
            {
                ime: st.ime,
                prezime: st.prezime,
                index: st.index,
            };

            var grupaDb = await grupa.findOne({ where: { naziv: st.grupa } });

            if (!grupaDb) {
                var novaGrupa =
                {
                    naziv: st.grupa
                };

                var gp = await grupa.create(novaGrupa);
                noviStudent.grupaId = gp.dataValues.id;
                var sp = await student.create(noviStudent);
            }
            else {
                noviStudent.grupaId = grupaDb['id'];
                var sp = await student.create(noviStudent);
            }

        }
    }
    if (postojeciStudenti === undefined || postojeciStudenti.length == 0) {
        res.status(200).json({ status: "Dodano " + nizStudenata.length + " studenata!" });
    }
    else {
        var m = nizStudenata.length - postojeciStudenti.length;
        var output = "";
        for (var i = 0; i < postojeciStudenti.length; i++) {
            if (i != postojeciStudenti.length - 1) {
                output = output + postojeciStudenti[i].index + ",";
            }
            else {
                output = output + postojeciStudenti[i].index;
            }

        }
        res.status(200).json({ status: "Dodano " + m + " studenata, a studenti " + output + " već postoje!" });
    }

});




//stare rute

app.get('/vjezbe', async (req, res) => {
    var vj = await zadatak.findOne({ include: vjezba });
    if (vj != null) {

        var brojVj = vj.vjezba.brojVjezbi;
        var brojZdtk = vj.brojZadataka;
        var s = [{
            brojVjezbi: brojVj,
            brojZadataka: brojZdtk
        }]

        var response = helpers.ParseVjezbe(s);
        res.status(200).json(response);
    }
    else {
        res.status(200).json([]);
    }

});


app.post('/vjezbe', async (req, res) => {
    var st = req.body;
    var bZ = st.brojZadataka.toString();
    var rows = [st.brojVjezbi.toString(), "{" + bZ.replaceAll(",", "|") + "}"];
    var stringex = "";
    var noviStringex = "";
    let brojZadatakaGreska = 0;
    let brojVjezbiGreska = 0;


    if (st.brojVjezbi < 0 || st.brojVjezbi > 15 || isNaN(st.brojVjezbi)) {
        brojVjezbiGreska = 1;
    }
    for (var i = 0; i < st.brojZadataka.length; i++) {
        if (st.brojZadataka[i] < 0 || st.brojZadataka[i] > 10 || isNaN(st.brojZadataka[i])) {
            stringex = stringex + "z" + st.brojZadataka[i].toString() + ", ";
            brojZadatakaGreska = 1;
            noviStringex = stringex.slice(0, -1);
        }
    }

    if (brojVjezbiGreska == 1) {
        res.status(400).json({
            status: "error",
            data: "Pogrešan parametar " + st.brojVjezbi.toString()
        });
    }
    else if (brojZadatakaGreska == 1) {
        res.status(400).json({
            status: "error",
            data: "Pogrešan parametar " + noviStringex
        });
    }
    else if (brojVjezbiGreska == 1 && brojZadatakaGreska == 1) {
        res.status(400).json({
            status: "error",
            data: "Pogrešan parametar " + st.brojVjezbi.toString() + ", " + noviStringex
        });
    }
    else if (st.brojVjezbi != st.brojZadataka.length) {
        res.status(400).json({
            status: "error",
            data: "Broj vjezbi se razlikuje od broja zadataka po vjezbi"
        });
    }
    else {
        await vjezba.destroy({ truncate: { cascade: true }, restartIdentity: true });
        await zadatak.destroy({ truncate: { cascade: false }, restartIdentity: true });
        var v = {
            brojVjezbi: rows[0]
        }
        var sv = await vjezba.create(v);
        var z = {
            brojZadataka: rows[1],
            vjezbaId: sv.dataValues.id
        }
        var sz = await zadatak.create(z);
        res.status(200).json(st);

    }
});


db.sequelize.sync({force:true}).then((req) => {
    app.listen(env, () => {
        console.log("SERVERE DIZI SE " + env);
    });
});
